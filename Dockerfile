FROM composer:2

WORKDIR /composer

RUN composer global require transip/tipctl -d /composer

ENV PATH=$PATH:/composer/vendor/bin

ENTRYPOINT ["tipctl"]
