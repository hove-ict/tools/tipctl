# TransIP CLI using Docker

## Setup

```shell
docker run --rm -ti -u "$(id -u):$(id -g)" -v "${HOME}/.config/transip-api:/.config/transip-api" registry.gitlab.com/hove-ict/tools/tipctl setup
```

This will create the file `~/.config/transip-api/cli-config.json` owned by your
user containing a TransIP API key with full access to everything in the set
account. Guard it well.

## Usage

Use the following command to list all available commands.

```shell
docker run --rm -ti -u "$(id -u):$(id -g)" -v "${HOME}/.config/transip-api:/.config/transip-api" registry.gitlab.com/hove-ict/tools/tipctl list
```

## Alias

Add the following to your shell's `.*rc` so you can just type `tipctl <command>`
to invoke the container with the key setup previously.

```shell
alias tipctl='docker run --rm -ti -u '"$(id -u):$(id -g)"' -v '"${HOME}"'/.config/transip-api:/.config/transip-api registry.gitlab.com/hove-ict/tools/tipctl'
```
